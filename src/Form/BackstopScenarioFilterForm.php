<?php

namespace Drupal\backstop_generator\Form;

use Drupal\Core\Form\FormStateInterface;

class BackstopScenarioFilterForm extends \Drupal\Core\Form\FormBase {

  /**
   * @inheritDoc
   */
  public function getFormId() {
    return 'entity_filter_form';
  }

  /**
   * @inheritDoc
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $request = \Drupal::request();

    $form['label_filter'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Filter by label'),
      '#default_value' => \Drupal::request()->query->get('label_filter'),
      '#size' => 30,
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Apply filter'),
    ];

    if ($request->getQueryString()) {
      $form['reset'] = [
        '#type' => 'submit',
        '#value' => $this->t('Reset'),
        '#attributes' => [
          'id' => 'reset-filter-button',
          'onclick' => 'document.getElementById("edit-label-filter").value=""; this.form.submit();',
        ],

      ];
    }

    $form['#method'] = 'get';
    $form['#action'] = \Drupal::request()->getRequestUri();

    return $form;
  }

  /**
   * @inheritDoc
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
  }

}
