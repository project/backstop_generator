<?php

namespace Drupal\backstop_generator\Form;

use Drupal\backstop_generator\Entity\BackstopProfile;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\InvokeCommand;
use Drupal\Core\Ajax\ReplaceCommand;
use Drupal\Core\Entity\EntityStorageException;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;
use Drupal\Core\Serialization\Yaml;
use Drupal\Core\Url;
use Drupal\node\Entity\Node;

/**
 * Configure BackstopJS settings for this site.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * @var array
   */
  protected array $menu_paths = [];

  protected array $config_map = [
    'backstop_directory',
    'test_domain',
    'reference_domain',
    'engine',
    'engineOptions',
    'default_profile' => [
      'include_homepage',
      'viewport_list',
//      'language_list',
//      'forms_list',
      'menu_list',
      'menu_depth',
      'node_quantity',
      'content_type_list',
      'path_list',
    ],
    'scenario_defaults' => [
      'delay',
      'hideSelectors',
      'removeSelectors',
      'misMatchThreshold',
      'requireSameDimensions',
      'cookiePath',
      'readyEvent',
      'readySelector',
      'readyTimeout',
      'onReadyScript',
      'onBeforeScript',
      'keyPressSelectors',
      'hoverSelector',
      'hoverSelectors',
      'clickSelector',
      'clickSelectors',
      'postInteractionWait',
      'scrollToSelector',
      'selectors',
      'selectorExpansion',
      'expect',
      'gotoParameters',
    ],
    'profile_parameters' => [
      'onBeforeScript',
      'paths',
      'report',
      'asyncCaptureLimit',
      'asyncCompareLimit',
//      'debug',
//      'debugWindow'
    ],
  ];

  /**
   * The 'Scenario defaults' description.
   *
   * @var string
   */
  protected string $scenarioDefaultsDesc = 'Each Profile consists of multiple Scenarios.
Each Scenario is a page on your site you want to test. The settings saved here will apply to all new Scenarios created. You can then edit and customize
the configuration of each Scenario as necessary. (see the Scenarios tab)';

  /**
   * The 'Profile parameters' description.
   *
   * @var string
   */
  protected string $profileParametersDesc = 'If desired, you can create multiple testing Profiles
to test specific areas of your site. The settings saved here will apply to all new Profiles created, but you can customize the configuration of each
Profile as necessary. (see the Profiles tab)';

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'backstop_generator_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['backstop_generator.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('backstop_generator.settings');

    // Attach the backstop_forms libraries.
    $form['#attached']['library'][] = 'backstop_generator/add-path-handler';
    $server = $_SERVER;
    $site = "{$server['REQUEST_SCHEME']}://{$server['SERVER_NAME']}";

    $viewport_url = Url::fromRoute('entity.backstop_viewport.add_form');
    $viewport_link = Link::fromTextAndUrl($this->t('Add a Viewport'), $viewport_url);

    $form['backstop_directory'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Backstop Directory'),
      '#description' => $this->t('This directory is in the <em>project</em> root, one level above your Drupal site.'),
      '#description_display' => 'before',
      '#default_value' => $config->get('backstop_directory') ?? '/tests/backstop',
      '#attributes' => [
        'disabled' => 'disabled',
      ]
    ];

    $form['test_domain'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Test domain'),
      '#description' => $this->t('The domain you want to test - no trailing slash.'),
      '#description_display' => 'before',
      '#default_value' => $config->get('test_domain') ?? $site,
      '#required' => TRUE,
      '#attributes' => [
        'placeholder' => 'http://domain-to-test.com',
      ]
    ];

    $form['reference_domain'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Reference domain'),
      '#description' => $this->t('The domain you want to test against (source of truth) - no trailing slash.'),
      '#description_display' => 'before',
      '#default_value' => $config->get('reference_domain') ?? '',
      '#required' => TRUE,
      '#attributes' => [
        'placeholder' => 'https://domain-to-reference.com',
      ]
    ];

    $form['default_profile'] = [
      '#type' => 'details',
      '#title' => $this->t('Default Profile'),
      '#description' => $this->t('Use this section to quickly create a default test Profile from menus, content types and paths.'),
      '#open' => FALSE,
      'viewports' => [
        '#type' => 'details',
        '#title' => $this->t('Viewports'),
        '#open' => TRUE,
      ],
      'scenarios' => [
        '#type' => 'details',
        '#title' => $this->t('Scenarios'),
        '#open' => FALSE,
        'include_homepage' => [
          '#type' => 'checkbox',
          '#title' => $this->t('Homepage'),
          '#default_value' => $config->get('default_profile.include_homepage') ?? TRUE,
          '#parents' => [
            'default_profile',
            'include_homepage',
          ],
          '#description' => $this->t('Check this box to include the homepage as a scenario.'),
        ],
        'menus' => [
          '#type' => 'details',
          '#title' => $this->t('Menus Items'),
          '#description' => $this->t('Create scenarios from menu links.'),
          '#open' => TRUE,
        ],
        'content_types' => [
          '#type' => 'details',
          '#title' => $this->t('Content Types'),
          '#description' => $this->t('Create scenarios of randomly selected content from content types.'),
          '#open' => TRUE,
        ],
        'paths' => [
          '#type' => 'details',
          '#title' => $this->t('Nodes and Paths'),
          '#description' => $this->t('Create scenarios from Nodes and specific paths (like Views pages).'),
          '#open' => TRUE,
        ],
      ],
    ];

    $form['scenario_defaults'] = [
      '#type' => 'details',
      '#title' => 'Scenario defaults',
      '#description' => $this->t($this->scenarioDefaultsDesc),
      '#open' => FALSE,
      'basic_settings' => [
        '#type' => 'details',
        '#title' => $this->t('Basic settings'),
        '#open' => FALSE,
      ],
      'advanced_settings' => [
        '#type' => 'details',
        '#title' => $this->t('Advanced settings'),
        '#open' => FALSE,
        'dom_events' => [
          '#type' => 'details',
          '#title' => $this->t('DOM events'),
          '#open' => FALSE,
          '#attributes' => ['id' => 'dom-events'],
        ],
        'user_behavior' => [
          '#type' => 'details',
          '#title' => $this->t('User behavior'),
          '#open' => FALSE,
        ]
      ]
    ];

    $form['default_profile']['viewports']['viewport_list'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Viewports'),
      '#description' => $this->t('Select the viewports to include in the default Profile'),
      '#description_display' => 'before',
      '#options' => $this->getConfig('backstop_viewport') ?? [],
      '#default_value' => $config->get('default_profile.viewport_list') ?? ['desktop','tablet','phone'],
      '#parents' => [
        'default_profile',
        'viewport_list',
      ],
      '#suffix' => $viewport_link->toString()->getGeneratedLink(),
    ];

    $form['default_profile']['scenarios']['menus']['menu_list'] = [
      '#type' => 'checkboxes',
      '#title' => 'Menus',
      '#description' => $this->t('Select which menus to include.'),
      '#description_display' => 'before',
      '#options' => $this->getAllMenus(),
      '#default_value' => $config->get('default_profile.menu_list') ?? ['main'],
      '#parents' => [
        'default_profile',
        'menu_list',
      ],
    ];

    $form['default_profile']['scenarios']['menus']['menu_depth'] = [
      '#type' => 'number',
      '#title' => $this->t('Menu depth'),
      '#description' => $this->t('Select the maximum depth of menu links to test - select 20 for all items.'),
      '#description_display' => 'before',
      '#required' => FALSE,
      '#min' => 0,
      '#max' => 20,
      '#step' => 1,
      '#default_value' => $config->get('default_profile.menu_depth') ?? 2,
      '#parents' => [
        'default_profile',
        'menu_depth',
      ],
    ];

    $form['default_profile']['scenarios']['content_types']['node_quantity'] = [
      '#type' => 'number',
      '#title' => $this->t('Number of each content type to test'),
      '#description' => $this->t('Enter the number of scenarios to create for each content type.'),
      '#description_display' => 'before',
      '#required' => FALSE,
      '#min' => 0,
      '#max' => 20,
      '#step' => 1,
      '#default_value' => $config->get('default_profile.node_quantity') ?? 5,
      '#parents' => [
        'default_profile',
        'node_quantity',
      ],
    ];

    $form['default_profile']['scenarios']['content_types']['content_type_list'] = [
      '#type' => 'checkboxes',
      '#title' => 'Node Types',
      '#description' => $this->t('Select which node types to include.'),
      '#description_display' => 'before',
      '#options' => $this->getAllContentTypes(),
      '#default_value' => $config->get('default_profile.content_type_list') ?? [],
      '#parents' => [
        'default_profile',
        'content_type_list',
      ],
    ];

    $form['default_profile']['scenarios']['paths']['node_title'] = [
      '#type' => 'entity_autocomplete',
      '#title' => $this->t('Node lookup'),
      '#description' => $this->t('Enter a node title, then click the \'Add path\' button to add it to the list of paths below.'),
      '#description_display' => 'before',
      '#target_type' => 'node',
      '#attributes' => ['id' => 'node-title']
    ];

    $form['default_profile']['scenarios']['paths']['add_button'] = [
      '#type' => 'button',
      '#value' => $this->t('Add path'),
      '#attributes' => ['id' => 'add-path-to-list'],
    ];

    $form['default_profile']['scenarios']['paths']['path_list'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Path list'),
      '#prefix' => '<div id="path-list-wrapper">',
      '#suffix' => '</div>',
      '#default_value' => $config->get('default_profile.path_list') ?? '',
      '#parents' => [
        'default_profile',
        'path_list',
      ],
      '#description' => $this->t('Add specific paths to test with labels separated by a pipe. (ex. label | path). One entry per line.' ),
      '#description_display' => 'before',
    ];

    $form['scenario_defaults']['basic_settings']['delay'] = [
      '#type' => 'number',
      '#title' => $this->t('delay'),
      '#description' => $this->t('The number of (milli)seconds to wait before testing.'),
      '#description_display' => 'before',
      '#min' => 0,
      '#max' => 10001,
      '#step' => 1,
      '#default_value' => $config->get('scenario_defaults.delay') ?? 0,
      '#parents' => [
        'scenario_defaults',
        'delay',
      ],
    ];

    $form['scenario_defaults']['basic_settings']['hideSelectors'] = [
      '#type' => 'textarea',
      '#title' => $this->t('hideSelectors'),
      '#description' => $this->t('Comma-separated list of CSS selectors that are set to display: none.'),
      '#description_display' => 'before',
      '#default_value' => $config->get('scenario_defaults.hideSelectors') ?? '',
      '#parents' => [
        'scenario_defaults',
        'hideSelectors',
      ],
    ];

    $form['scenario_defaults']['basic_settings']['removeSelectors'] = [
      '#type' => 'textarea',
      '#title' => $this->t('removeSelectors'),
      '#description' => $this->t('Comma-separated list of CSS Selectors that are set to visibility: none.'),
      '#description_display' => 'before',
      '#default_value' => $config->get('scenario_defaults.removeSelectors') ?? '',
      '#parents' => [
        'scenario_defaults',
        'removeSelectors',
      ],
    ];

    $form['scenario_defaults']['basic_settings']['misMatchThreshold'] = [
      '#type' => 'number',
      '#title' => $this->t('misMatchThreshold'),
      '#description' => $this->t('Set the trigger threshold as a percentage - 0.1 = 0.1% (default is 0.1.'),
      '#description_display' => 'before',
      '#min' => 0,
      '#max' => 100,
      '#step' => .01,
      '#default_value' => $config->get('scenario_defaults.misMatchThreshold') ?? .10,
      '#parents' => [
        'scenario_defaults',
        'misMatchThreshold',
      ],
    ];

    $form['scenario_defaults']['basic_settings']['requireSameDimensions'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('requireSameDimensions'),
      '#description' => $this->t('Check this to confirm that reference and test captures are the exact same dimensions.'),
      '#description_display' => 'before',
      '#default_value' => $config->get('scenario_defaults.requireSameDimensions') ?? TRUE,
      '#parents' => [
        'scenario_defaults',
        'requireSameDimensions',
      ],
    ];

    $form['scenario_defaults']['advanced_settings']['cookiePath'] = [
      '#type' => 'textfield',
      '#title' => $this->t('cookiePath') ?? '/this/cookie/file/path',
      '#default_value' => $config->get('scenario_defaults.cookiePath'),
      '#parents' => [
        'scenario_defaults',
        'cookiePath',
      ],
      '#description' => $this->t('Import cookies in JSON format to get around the <em>Accept Cookies</em> screen.'),
      '#description_display' => 'before',
    ];

    $form['scenario_defaults']['advanced_settings']['dom_events']['readyEvent'] = [
      '#type' => 'textfield',
      '#title' => $this->t('readyEvent'),
      '#default_value' => $config->get('scenario_defaults.readyEvent'),
      '#parents' => [
        'scenario_defaults',
        'readyEvent',
      ],
      '#description' => $this->t('Wait until this string has been logged to the console.'),
      '#description_display' => 'before',
    ];

    $form['scenario_defaults']['advanced_settings']['dom_events']['readySelector'] = [
      '#type' => 'textfield',
      '#title' => $this->t('readySelector'),
      '#default_value' => $config->get('scenario_defaults.readySelector'),
      '#parents' => [
        'scenario_defaults',
        'readySelector',
      ],
      '#description' => $this->t('Wait until this selector exists before continuing.'),
      '#description_display' => 'before',
    ];

    $form['scenario_defaults']['advanced_settings']['dom_events']['readyTimeout'] = [
      '#type' => 'number',
      '#title' => $this->t('readyTimeout'),
      '#default_value' => $config->get('scenario_defaults.readyTimeout'),
      '#parents' => [
        'scenario_defaults',
        'readyTimeout',
      ],
      '#description' => $this->t('Timeout for readyEvent and readySelector.'),
      '#field_suffix' => 'ms',
      '#description_display' => 'before',
    ];

    // todo: create a custom, multi-column field.
    $form['scenario_defaults']['advanced_settings']['user_behavior']['keyPressSelectors'] = [
      '#type' => 'textarea',
      '#title' => $this->t('keyPressSelectors'),
      '#default_value' => $config->get('scenario_defaults.keyPressSelectors'),
      '#parents' => [
        'scenario_defaults',
        'keyPressSelectors',
      ],
      '#description' => $this->t('List of selectors to simulate multiple sequential keypress interactions.'),
      '#description_display' => 'before',
    ];

    $form['scenario_defaults']['advanced_settings']['user_behavior']['hoverSelector'] = [
      '#type' => 'textfield',
      '#title' => $this->t('hoverSelector'),
      '#default_value' => $config->get('scenario_defaults.hoverSelector'),
      '#parents' => [
        'scenario_defaults',
        'hoverSelector',
      ],
      '#description' => $this->t('Move the pointer over the specified DOM element prior to screen shot.'),
      '#description_display' => 'before',
    ];

    $form['scenario_defaults']['advanced_settings']['user_behavior']['hoverSelectors'] = [
      '#type' => 'textarea',
      '#title' => $this->t('hoverSelectors'),
      '#default_value' => $config->get('scenario_defaults.hoverSelectors'),
      '#parents' => [
        'scenario_defaults',
        'hoverSelectors',
      ],
      '#description' => $this->t('Selectors to simulate multiple sequential hover interactions.'),
      '#description_display' => 'before',
    ];

    $form['scenario_defaults']['advanced_settings']['user_behavior']['clickSelector'] = [
      '#type' => 'textfield',
      '#title' => $this->t('clickSelector'),
      '#default_value' => $config->get('scenario_defaults.clickSelector'),
      '#parents' => [
        'scenario_defaults',
        'clickSelector',
      ],
      '#description' => $this->t('Click the specified DOM element prior to screen shot.'),
      '#description_display' => 'before',
    ];

    $form['scenario_defaults']['advanced_settings']['user_behavior']['clickSelectors'] = [
      '#type' => 'textarea',
      '#title' => $this->t('clickSelectors'),
      '#default_value' => $config->get('scenario_defaults.clickSelectors'),
      '#parents' => [
        'scenario_defaults',
        'clickSelectors',
      ],
      '#description' => $this->t('Description of the backstop scenario.'),
      '#description_display' => 'before',
    ];

    $form['scenario_defaults']['advanced_settings']['user_behavior']['postInteractionWait'] = [
      '#type' => 'number',
      '#title' => $this->t('postInteractionWait'),
      '#default_value' => $config->get('scenario_defaults.postInteractionWait'),
      '#parents' => [
        'scenario_defaults',
        'postInteractionWait',
      ],
      '#description' => $this->t('Wait for a selector after interacting with hoverSelector or clickSelector.'),
      '#description_display' => 'before',
    ];

    $form['scenario_defaults']['advanced_settings']['scrollToSelector'] = [
      '#type' => 'textfield',
      '#title' => $this->t('scrollToSelector'),
      '#default_value' => $config->get('scenario_defaults.scrollToSelector'),
      '#parents' => [
        'scenario_defaults',
        'scrollToSelector',
      ],
      '#description' => $this->t('Scrolls the specified DOM element into view prior to screen shot.'),
      '#description_display' => 'before',
    ];

    $form['scenario_defaults']['advanced_settings']['selectors'] = [
      '#type' => 'textarea',
      '#title' => $this->t('selectors'),
      '#default_value' => $config->get('scenario_defaults.selectors'),
      '#parents' => [
        'scenario_defaults',
        'selectors',
      ],
      '#description' => $this->t('List of selectors to capture.'),
      '#description_display' => 'before',
    ];

    $form['scenario_defaults']['advanced_settings']['selectorExpansion'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('selectorExpansion'),
      '#default_value' => $config->get('scenario_defaults.selectorExpansion') ?? FALSE,
      '#parents' => [
        'scenario_defaults',
        'selectorExpansion',
      ],
      '#description' => $this->t('Whether to take screenshots of designated selectors.'),
      '#return_value' => 1,
    ];

    $form['scenario_defaults']['advanced_settings']['expect'] = [
      '#type' => 'number',
      '#title' => $this->t('expect'),
      '#default_value' => $config->get('scenario_defaults.expect'),
      '#parents' => [
        'scenario_defaults',
        'expect',
      ],
      '#description' => $this->t('The number of selector elements to test for.'),
      '#description_display' => 'before',
    ];

    // todo: custom field with multiple columns for goToParameter values.
    $form['scenario_defaults']['advanced_settings']['gotoParameters'] = [
      '#type' => 'textfield',
      '#title' => $this->t('gotoParameters'),
      '#default_value' => $config->get('scenario_defaults.gotoParameters'),
      '#parents' => [
        'scenario_defaults',
        'gotoParameters',
      ],
      '#description' => $this->t('A list of settings passed to page.goto(url, parameters) function.'),
      '#description_display' => 'before',
    ];

    $form['profile_parameters'] = [
      '#type' => 'details',
      '#title' => $this->t('Profile parameters'),
      '#description' => $this->t($this->profileParametersDesc),
      '#open' => FALSE,
      '#attributes' => ['id' => 'profile-parameters'],
    ];

    $form['profile_parameters']['paths'] = [
      '#type' => 'textarea',
//      '#title' => $this->t('Paths'),
//      '#description' => $this->t('TODO: Need description here.'),
//      '#description_display' => 'before',
      '#default_value' => $config->get('profile_parameters.paths'),
      '#parents' => [
        'profile_parameters',
        'paths',
      ],
      '#attributes' => [
        'class' => ['advanced-setting hidden-field'],
      ],
    ];

    $form['profile_parameters']['report'] = [
      '#type' => 'textfield',
//      '#title' => $this->t('Report'),
//      '#description' => $this->t('TODO: Need description here.'),
//      '#description_display' => 'before',
      '#default_value' => $config->get('profile_parameters.report') ?? 'browser',
      '#parents' => [
        'profile_parameters',
        'report',
      ],
      '#attributes' => [
        'class' => ['advanced-setting'],
        'hidden' => TRUE,
      ],
    ];

    $form['profile_parameters']['asyncCaptureLimit'] = [
      '#type' => 'number',
      '#title' => $this->t('asyncCaptureLimit'),
      '#description' => $this->t('Set the number of concurrent capture sessions to run - default is 5.'),
      '#description_display' => 'before',
      '#default_value' => $config->get('profile_parameters.asyncCaptureLimit') ?? 5,
      '#parents' => [
        'profile_parameters',
        'asyncCaptureLimit',
      ],
      '#attributes' => [
        'class' => ['advanced-setting'],
      ],
    ];

    $form['profile_parameters']['asyncCompareLimit'] = [
      '#type' => 'number',
      '#title' => $this->t('asyncCompareLimit'),
      '#description' => $this->t('Set the number of concurrent comparisons to run - default is 50.'),
      '#description_display' => 'before',
      '#default_value' => $config->get('profile_parameters.asyncCompareLimit') ?? 50,
      '#parents' => [
        'profile_parameters',
        'asyncCompareLimit',
      ],
      '#attributes' => [
        'class' => ['advanced-setting'],
      ],
    ];

//    $form['profile_parameters']['debugging'] = [
//      '#type' => 'details',
//      '#title' => $this->t('Debugging'),
//      '#open' => FALSE,
//    ];

//    $form['profile_parameters']['debugging']['debug'] = [
//      '#type' => 'checkbox',
//      '#title' => $this->t('debug'),
//      '#description' => $this->t('Enable debug mode.'),
//      '#description_display' => 'before',
//      '#default_value' => $config->get('profile_parameters.debug') ?? FALSE,
//      '#parents' => [
//        'profile_parameters',
//        'debug',
//      ],
//    ];

//    $form['profile_parameters']['debugging']['debugWindow'] = [
//      '#type' => 'checkbox',
//      '#title' => $this->t('debugWindow'),
//      '#description' => $this->t('Enable debug windows.'),
//      '#description_display' => 'before',
//      '#default_value' => $config->get('profile_parameters.debugWindow') ?? FALSE,
//      '#parents' => [
//        'profile_parameters',
//        'debugWindow',
//      ],
//    ];

    $form['engine_options'] = [
      '#type' => 'details',
      '#title' => $this->t('Engine options'),
      '#description' => $this->t('<a href="https://pptr.dev/guides/what-is-puppeteer">Puppeteer</a> and <a href="https://playwright.dev/">Playwright</a> are JavaScript libraries for testing pages in browswers.'),
      '#description_display' => 'before',      '#open' => FALSE,
    ];

    $form['engine_options']['engine'] = [
      '#type' => 'radios',
      '#title' => 'engine',
      '#default_value' => $config->get('engine') ?? 'puppeteer',
      '#options' => [
        'puppeteer' => 'Puppeteer',
        'playwright' => 'Playwright',
      ],
      '#ajax' => [
        'callback' => '::modifyScriptOptions',
        'event' => 'change',
      ]
    ];

    $form['engine_options']['engineOptions'] = [
      '#type' => 'textfield',
      '#title' => $this->t('engineOptions'),
      '#description' => $this->t('TODO: Need description here.'),
      '#description_display' => 'before',
      '#default_value' => $config->get('engineOptions') ?? '--no-sandbox',
    ];

    $engine = $form['engine_options']['engine']['#default_value'];
    $onBeforeDefault = $config->get('scenario_defaults.onBeforeScript') ?? '';
    $onReadyDefault = $config->get('scenario_defaults.onReadyScript') ?? '';
    $profileOnBeforeDefault = $config->get('profile_parameters.onBeforeScript') ?? '';

    $form['scenario_defaults']['advanced_settings']['dom_events']['onBeforeScript'] = $this->scriptOptions('onBefore', $engine, $onBeforeDefault);

    $form['scenario_defaults']['advanced_settings']['dom_events']['onReadyScript'] = $this->scriptOptions('onReady', $engine, $onReadyDefault);

    $form['profile_parameters']['onBeforeScript'] = $this->scriptOptions('onBefore', $engine, $profileOnBeforeDefault);

    switch ($form_state->getValue('engine')) {
      case 'puppeteer':
        // Scenario onBefore and onReady scripts.
        $form['scenario_defaults']['advanced_settings']['dom_events']['onReadyScript']['#options'] = [
          '' => $this->t('Do not include'),
          'puppet/onReady.js' => $this->t('puppet/onReady.js'),
        ];
        $form['scenario_defaults']['advanced_settings']['dom_events']['onBeforeScript']['#options'] = [
          '' => $this->t('Do not include'),
          'puppet/onBefore.js' => $this->t('puppet/onBefore.js'),
        ];
        $form['scenario_defaults']['advanced_settings']['dom_events']['onReadyScript']['#default_value'] = '';
        $form['scenario_defaults']['advanced_settings']['dom_events']['onBeforeScript']['#default_value'] = '';

        // Profile onBeforeScript.
        $form['profile_parameters']['onBeforeScript']['#options'] = [
          '' => $this->t('Do not include'),
          'puppet/onBefore.js' => $this->t('puppet/onBefore.js'),
        ];
        $form['profile_parameters']['onBeforeScript']['#default_value'] = '';
        break;

      case 'playwright':
        // Scenario onBefore and onReady scripts.
        $form['scenario_defaults']['advanced_settings']['dom_events']['onReadyScript']['#options'] = [
          '' => $this->t('Do not include'),
          'playwright/onReady.js' => $this->t('playwright/onReady.js'),
        ];
        $form['scenario_defaults']['advanced_settings']['dom_events']['onBeforeScript']['#options'] = [
          '' => $this->t('Do not include'),
          'playwright/onBefore.js' => $this->t('playwright/onBefore.js'),
        ];
        $form['scenario_defaults']['advanced_settings']['dom_events']['onReadyScript']['#default_value'] = '';
        $form['scenario_defaults']['advanced_settings']['dom_events']['onBeforeScript']['#default_value'] = '';

        // Profile onBeforeScript.
        $form['profile_parameters']['onBeforeScript']['#options'] = [
          '' => $this->t('Do not include'),
          'playwright/onBefore.js' => $this->t('playwright/onBefore.js'),
        ];
        $form['profile_parameters']['onBeforeScript']['#default_value'] = '';
        break;
    }

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    /** @var \Drupal\Core\File\FileSystemInterface $file_system */
    $file_system = \Drupal::service('file_system');

    // If a new directory is created, delete the old directory.
    $current_dir = $this->config('backstop_generator.settings')
      ->get('backstop_directory');
    $new_dir = $form_state->getValue('backstop_directory');
    if ($current_dir && $current_dir != $new_dir) {
      // Create the backstop directory where profiles will be saved.
      $project_root = dirname(DRUPAL_ROOT);
      $profile_directory = "$project_root/{$form_state->getValue('backstop_directory')}";
      $file_system->prepareDirectory(
        $profile_directory,
        FileSystemInterface::CREATE_DIRECTORY | FileSystemInterface::MODIFY_PERMISSIONS
      );
    }

    $config = $this->config('backstop_generator.settings');

    foreach ($this->config_map as $config_key => $config_val) {
      // Handle parent groups.
      if (is_array($config_val)) {

        foreach ($config_val as $config_name) {
          $value = $form_state->getValue([$config_key, $config_name]);
//          // Substitute the onBefore and onReady boolean field values.
//          if ($value !== null && $value !== '') {
//            if (
//              $value != 0 &&
//              ($config_name == 'onBeforeScript' || $config_name == 'onReadyScript')
//            ) {
//              $script = $config_name == 'onBeforeScript' ? 'puppet/onBefore.js' : 'puppet/onReady.js';
//              $config->set("$config_key.$config_name", $script);
//              continue;
//            }

//          if ($value == '') {
//            $config->set("$config_key.$config_name", NULL);
//          } else {
            $config->set("$config_key.$config_name", $form_state->getValue([$config_key, $config_name]));
//          }

//          }
        }
      }
      // Handle root level field values.
      else {
        $config->set($config_val, $form_state->getValue($config_val));
      }
    }
    $config->save();

    // CREATE THE SCENARIOS.
    // Get the main Backstop settings.
    $backstopSettings = $this->configFactory->get('backstop_generator.settings');

    // Set the properties for all scenarios.
    $properties = [
      'url' => '',
      'referenceUrl' => '',
      'useScenarioDefaults' => TRUE,
    ];

    // Delete previous default scenarios.
    $scenarios = $this->configFactory->listAll('backstop_generator.scenario.default_');

    foreach ($scenarios as $scenario) {
      $this->configFactory->getEditable($scenario)->delete();
    }

    // Create the new scenarios.
    if ($form_state->getValue('include_homepage')) {
      $this->createScenarios($properties, $form_state, 'homepage');
    }
    $this->createScenarios($properties, $form_state, 'menus');
    $this->createScenarios($properties, $form_state, 'content_types');
    $this->createScenarios($properties, $form_state, 'paths');

    // CREATE THE PROFILE.
    // Get the default scenarios.
    $new_scenarios = $this->configFactory->listAll('backstop_generator.scenario.default_');
    $new_scenario_ids = [];
    foreach ($new_scenarios as $new_scenario) {
      preg_match('/\w+\.\w+\.([\w_-]+)/', $new_scenario, $id);
      $new_scenario_ids[] = $id[1];
    }

    // Delete the current default profile.
    $this->configFactory
      ->getEditable('backstop_generator.profile.default')
      ?->delete();

    $profile = [
      'id' => 'default',
      'label' => 'Default Test',
      'status'=> TRUE,
      'description' => 'The default test script.',
      'useProfileDefaults' => TRUE,
      'viewports' => $backstopSettings->get('default_profile.viewport_list'),
      'onBeforeScript' => $backstopSettings->get('profile_parameters.onBeforeScript'),
      'scenarios' => $new_scenario_ids,
      'paths' => $backstopSettings->get('profile_parameters.paths'),
      'report' => $backstopSettings->get('profile_parameters.report'),
      'engine' => $backstopSettings->get('engine'),
      'engineOptions' => $backstopSettings->get('engineOptions'),
      'asyncCaptureLimit' => $backstopSettings->get('profile_parameters.asyncCaptureLimit'),
      'debug' => $backstopSettings->get('profile_parameters.debug'),
      'debugWindow' => $backstopSettings->get('profile_parameters.debugWindow'),
    ];
    // Load the entity type manager.
    $entity_type_manager = \Drupal::entityTypeManager();
    /** @var \Drupal\backstop_generator\Entity\BackstopProfile $backstopReport */
    $backstopProfile = $entity_type_manager->getStorage('backstop_profile')->create($profile);
    $backstopProfile->save();
    $backstopProfile->generateBackstopFile('default');

    parent::submitForm($form, $form_state);

    // Update the backstop.json files for profiles that use global settings.
    $updated_profiles = $this->updateProfiles();
    // Inform the user which backstop.json files have been updated.
    $message = count($updated_profiles) > 1 ?
      $this->t('The %label backstop.json files have been updated.', ['%label' => implode(', ', $updated_profiles), ]) :
      $this->t('The %label backstop.json file has been updated.', ['%label' => implode(', ', $updated_profiles), ]);
    $this->messenger->addMessage($message);
  }

  /**
   * Regenerate the backstop.json files for Profiles that use global settings.
   *
   * @return array
   *   Array of profile names that were updated.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  private function updateProfiles() {
    $profile_ids = \Drupal::entityTypeManager()
      ->getStorage('backstop_profile')
      ->getQuery()
      ->execute();
    $settings = $this->config('backstop_generator.settings');
    $regenerated_profiles = [];

    if (!empty($profile_ids)) {
      foreach ($profile_ids as $id) {
        // Load the profile config.
        $profile_config = \Drupal::configFactory()->getEditable("backstop_generator.profile.$id");

        // Ignore profiles using custom advanced settings.
        if (!$profile_config->get('use_globals')) {
          continue;
        }

        // Update the config values and save.
        $profile_config->set('onBeforeScript', $settings->get('onBeforeScript'))
          ->set('paths', $settings->get('paths'))
          ->set('report', $settings->get('report'))
          ->set('engine', $settings->get('engine'))
          ->set('engineOptions', $settings->get('engineOptions'))
          ->set('asyncCaptureLimit', $settings->get('asyncCaptureLimit'))
          ->set('asyncCompareLimit', $settings->get('asyncCompareLimit'));
        $profile_config->save();

        // Regenerate the backstop.json file.
        $profile = BackstopProfile::load($id);
        $profile->generateBackstopFile($id);
        $regenerated_profiles[] = $profile->label();
      }
    }
    return $regenerated_profiles;
  }

  /**
   * Creates and saves a Backstop Scenario configuration entity.
   *
   * @param string $id
   *   The unique ID of the backstop scenario.
   * @param string $label
   *   The label of the backstop scenario.
   * @param array $properties
   *   An associative array of additional properties to set on the entity.
   *
   * @return \Drupal\Core\Entity\EntityInterface|false
   *   The saved entity, or FALSE on failure.
   */
  protected function createBackstopScenario($id, $label, array $properties = []) {
    try {
      // Load the entity type manager.
      $entity_type_manager = \Drupal::entityTypeManager();

      // Check if an entity with the same URL already exists.
      if (isset($properties['url'])) {
        $existing_entities = $entity_type_manager->getStorage('backstop_scenario')->loadByProperties(['url' => $properties['url']]);
        if ($existing_entities) {
          $url = parse_url($properties['url']);
          if (isset($url['path'])) {
            $this->messenger()->addStatus("A Backstop Scenario for URL {$url['path']} was skipped because it already exists.");
            return FALSE;
          }
        }
      }

      // Create a new entity instance.
      /** @var \Drupal\backstop_generator\Entity\BackstopScenario $entity */
      $entity = $entity_type_manager->getStorage('backstop_scenario')->create([
          'id' => $id,
          'label' => $label,
        ] + $properties);

      // Save the entity.
      $entity->save();

      return $entity;
    }
    catch (EntityStorageException $e) {
      \Drupal::logger('backstop_generator')->error('Failed to save backstop scenario entity: @message', ['@message' => $e->getMessage()]);
      return FALSE;
    }
  }

  /**
   * Get all content types in the system.
   *
   * @return array
   *   An array of all content types.
   */
  private function getAllContentTypes(): array {
    $entityTypeManager = \Drupal::entityTypeManager();
    $content_types = [];
    $node_type_storage = $entityTypeManager->getStorage('node_type');
    $types = $node_type_storage->loadMultiple();

    foreach ($types as $type) {
      $content_types[$type->id()] = $type->label();
    }

    return $content_types;
  }

  /**
   * Get all menus in the system.
   *
   * @return array
   *   An array of all menus.
   */
  public function getAllMenus(): array {
    $entityTypeManager = \Drupal::entityTypeManager();
    $menu_storage = $entityTypeManager->getStorage('menu');
    $menus = $menu_storage->loadMultiple();
    $menu_list = [];

    foreach ($menus as $menu) {
      $menu_list[$menu->id()] = $menu->label();
    }

    return $menu_list;
  }

  /**
   * Returns an array of config labels.
   *
   * @param string $config_name
   *   The name of the configuration entity (ex. backstop_viewport).
   *
   * @return array
   *   The list of config labels keyed by the config ID.
   */
  protected function getConfig(string $config_name) {
    // Get the config entity manager.
    $entity_storage = \Drupal::service('entity_type.manager')
      ->getStorage($config_name);
    // Get the entity query object.
    $entity_query = $entity_storage->getQuery();
    $config_ids = $entity_query->execute();
    // Load the config entities.
    $configs = $entity_storage->loadMultiple($config_ids);

    // Create the array of configs.
    $config_list = [];
    foreach ($configs as $config) {
      if ($config_name == 'backstop_scenario') {
        $config_list[$config->id()] = ucfirst($config->get('bundle')) . ": {$config->label()}";
        continue;
      }
      $config_list[$config->id()] = $config->label();
    }
    asort($config_list);
    return $config_list;
  }

  /**
   * Processes the incoming data to create test scenarios.
   *
   * @param array $properties
   *   Associative array of Sscenario entity properties.
   * @param FormStateInterface $form_state
   *   The form_state values.
   * @param string $source
   *   A name representing where the Scenario data is coming from.
   * @param bool $createProfile
   *   Used to determine if a Profile will be created in addition to Scenarios.
   *
   * @return void
   */
  protected function createScenarios(array $properties, FormStateInterface $form_state, string $source, bool $createProfile = TRUE) {
    $id_prefix = $createProfile ? 'default_' : '';

    switch ($source) {
      case 'homepage':
        $siteSettings = $this->configFactory->get('system.site');
        $properties['url'] = $form_state->getValue('test_domain') . '/';
        $properties['referenceUrl'] = $form_state->getValue('reference_domain') . '/';
        $properties['bundle'] = 'front';
        $this->createBackstopScenario($id_prefix . 'home', 'Home', $properties);
        break;

      case 'menus':
        // Get the scenario paths from the menus.
        /** @var \Drupal\backstop_generator\Services\MenuNodeData $backstopMenuService */
        $backstopMenuService = \Drupal::service('backstop_generator.menu_node_data');

        $menus = $form_state->getValue(['default_profile', 'menu_list']);
        $menu_depth = $form_state->getValue(['default_profile', 'menu_depth']) === 0 ? 100 : $form_state->getValue(['default_profile', 'menu_depth']);

        foreach ($menus as $menu) {
          if ($menu != 0) {
            $this->menu_paths[$menu] = $backstopMenuService->getMenuLinkPaths($menu, $menu_depth);
          }
        }

        // Create the scenarios based on menu paths.
        foreach ($this->menu_paths as $menu => $menu_paths) {
          foreach ($menu_paths as $menu_path) {
            $path = $menu_path['path'] == '/' ?
              'home' :
              str_replace('/', '-', substr($menu_path['path'], 1));

            // Clean the path to remove characters not allowed in a config entity name.
            $path = preg_replace('/[:?*<>"\'\/\\\\]/', '', $path);

            $properties['url'] = $form_state->getValue('test_domain') . $menu_path['path'];
            $properties['referenceUrl'] = $form_state->getValue('reference_domain') . $menu_path['path'];
            $properties['bundle'] = $menu_path['bundle'];
            $label = "menu:$menu:{$menu_path['bundle']}:{$menu_path['title']}";
              $this->createBackstopScenario($id_prefix . $path, $label, $properties);
          }
        }
        break;

      case 'content_types':
        // Get the scenario paths from the content types.
        /** @var \Drupal\backstop_generator\Services\RandomNodeList $randomNodeService */
        $randomNodeService = \Drupal::service('backstop_generator.random_node_list');

        $content_types = $form_state->getValue(['default_profile', 'content_type_list']);
        $scenarioQuantity = $form_state->getValue(['default_profile', 'node_quantity']);
        $content_type_scenarios = [];

        foreach ($content_types as $content_type) {
          if ($content_type != 0) {
            $content_type_scenarios = array_merge($content_type_scenarios, $randomNodeService->getRandomNodes($content_type, $scenarioQuantity));
          }
        }

        // Create the scenarios based on content type.
        foreach ($content_type_scenarios as $node) {
          $id = "node-{$node['nid']}";
          $properties['url'] = $form_state->getValue('test_domain') . "/node/{$node['nid']}";
          $properties['referenceUrl'] = $form_state->getValue('reference_domain') . "/node/{$node['nid']}";
          $properties['bundle'] = $node['bundle'];
          $label = "node:{$node['bundle']}:{$node['title']}";
          $this->createBackstopScenario($id_prefix . $id, $label, $properties);
        }
        break;

      case 'paths':
        // Get the scenario paths specified by the user.
        unset($properties['bundle']);

        $specific_paths = explode("\r\n", $form_state->getValue(['default_profile', 'path_list'])) ?? [];

        if (!empty($specific_paths)) {
          foreach ($specific_paths as $path) {
            if (empty($path)) {
              continue;
            }

            // Unset the bundle key from the previous iteration.
            unset($properties['bundle']);

            // Get the node information if it exists.
            if (preg_match('/node\/(\d+)/', $path, $node_path)) {
              $node = Node::load($node_path[1]);
              $properties['bundle'] = $node->bundle();
            }
            else { // Get the internal path (node/[nid]) from an alias.
              /** @var \Drupal\path_alias\AliasManagerInterface $aliasManager */
              $aliasManager = \Drupal::service('path_alias.manager');

              $path = $aliasManager->getPathByAlias($path);
            }

            // Parse the 'Label | node/1' format into parts.
            preg_match('/(.+)\s\|\s(.+)/', $path, $path_matches);
            $label = "path:{$path_matches[1]}";
            $test_path = $path_matches[2];

            $id = isset($properties['bundle']) ?
              // Replace slashes with hyphens for typical path.
              str_replace('/', '-', $test_path) :
              // Replace the path with a hash for paths with query strings.
              substr(md5($path_matches[0]), 0, 9);

            $properties['url'] = $form_state->getValue('test_domain') . "/$test_path";
            $properties['referenceUrl'] = $form_state->getValue('reference_domain') . "/$test_path";

            $this->createBackstopScenario($id_prefix . $id, $label, $properties);
          }
        }
        break;
    }
  }

  /**
   * Returns the array of config names indicated by the $key.
   *
   * @param string|NULL $key
   *   The name of the $config_map key to return.
   *   NULL returns the whole array.
   *
   * @return array|mixed

  public function getConfigMap(string $key = NULL) {
    return $key ? $this->config_map[$key] : $this->config_map;
  }*/

  /**
   * Returns the onBefore/onReady script options field.
   *
   * @param string $file_name
   *   The name of the script file (onBefore || onReady).
   * @param string $engine_name
   *   The value of the engine used (puppeteer || playwright)
   * @param string $default_value
   *   The field's default value.
   *
   * @return array
   */
  protected function scriptOptions (string $file_name, string $engine_name, string $default_value) {
    $is_profile = str_contains($default_value, 'profile');
    $field = [
      '#type' => 'radios',
      '#title' => $this->t("{$file_name}Script"),
      '#description' => $this->t('Used to set up browser state e.g. cookies.'),
      '#description_display' => 'before',
      '#default_value' => $default_value ?? '',
    ];

    // Options based on engine name.
    switch ($engine_name) {
      case 'puppeteer':
        $field['#options'] = [
          '' => $this->t('Do not include'),
          "puppet/$file_name.js" => "puppet/$file_name.js",
        ];
        break;

      case 'playwright':
        $field['#options'] = [
          '' => $this->t('Do not include'),
          "playwright/$file_name.js" => "playwright/$file_name.js",
        ];
        break;
    }

    // Parent declaration based on default_value value.
    $field['#parents'] = match ($is_profile) {
      TRUE => [
        'profile_parameters',
        "{$file_name}Script"
      ],
      FALSE => [
        'scenario_defaults',
        "{$file_name}Script"
      ],
    };
    return $field;
  }

  /**
   * AJAX callback to modify the onBefore and onReady script options.
   *
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *
   * @return \Drupal\Core\Ajax\AjaxResponse
   */
  public function modifyScriptOptions (array $form, FormStateInterface $form_state) {
    $response = new AjaxResponse();
    $response->addCommand(new ReplaceCommand('#dom-events', $form['scenario_defaults']['advanced_settings']['dom_events']));
    $response->addCommand(new ReplaceCommand('#profile-parameters', $form['profile_parameters']));

    return $response;
  }
}
