<?php

namespace Drupal\backstop_generator\Form;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\InvokeCommand;
use Drupal\Core\Ajax\ReplaceCommand;
use Drupal\Core\Config\Entity\ConfigEntityStorage;
use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;
use Drupal\Core\Url;

/**
 * Backstop Profile form.
 *
 * @property \Drupal\backstop_generator\BackstopProfileInterface $entity
 */
class BackstopProfileForm extends EntityForm {

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {

    $scenario_url = Url::fromRoute('entity.backstop_scenario.add_form');
    $scenario_link = Link::fromTextAndUrl($this->t('Add a Scenario'), $scenario_url);
    $viewport_url = Url::fromRoute('entity.backstop_viewport.add_form');
    $viewport_link = Link::fromTextAndUrl($this->t('Add a Viewport'), $viewport_url);
    $backstop_config = \Drupal::config('backstop_generator.settings');

    $form = parent::form($form, $form_state);

    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => $this->entity->label(),
      '#description' => $this->t('Label for the backstop profile.'),
      '#required' => TRUE,
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $this->entity->id(),
      '#machine_name' => [
        'exists' => '\Drupal\backstop_generator\Entity\BackstopProfile::load',
      ],
      '#disabled' => !$this->entity->isNew(),
    ];

    $form['useProfileDefaults'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Use Profile defaults'),
      '#default_value' => $this->entity->get('useProfileDefaults') ?? TRUE,
    ];

    $form['profile_settings'] = [
      '#type' => 'details',
      '#title' => $this->t('Profile Settings'),
      '#open' => TRUE,
      '#attributes' => [
        'id' => 'profile-settings',
        'class' => ['advanced-settings'],
      ],
      '#states' => [
        'visible' => [
          ':input[name="useProfileDefaults"]' => ['checked' => FALSE],
        ],
      ],
    ];

    $form['profile_settings']['engine'] = [
      '#type' => 'radios',
      '#title' => $this->t('Engine'),
      '#description' => $this->t('TODO: Need description here.'),
      '#description_display' => 'before',
      '#default_value' => $this->entity->get('engine') ?? $backstop_config->get('profile_parameters.engine'),
      '#options' => [
        'puppeteer' => $this->t('Puppeteer'),
        'playwright' => $this->t('Playwright'),
      ],
      '#ajax' => [
        'callback' => '::modifyScriptOption',
        'event' => 'change',
      ]
    ];

    $form['profile_settings']['engineOptions'] = [
      '#type' => 'textfield',
      //      '#title' => $this->t('engineOptions'),
      //      '#description' => $this->t('TODO: Need description here.'),
      //      '#description_display' => 'before',
      '#default_value' => $this->entity->get('engineOptions') ?? $backstop_config->get('profile_parameters.engineOptions'),
      '#attributes' => [
        //        'readonly' => $this->entity->get('use_globals') ?? TRUE,
        'class' => ['advanced-setting'],
        'hidden' => TRUE,
      ],
    ];

    $form['profile_settings']['onBeforeScript'] = $this->onBeforeScriptOption($form['profile_settings']['engine']['#default_value']);

    $form['profile_settings']['paths'] = [
      '#type' => 'textarea',
      //      '#title' => $this->t('Paths'),
      //      '#description' => $this->t('TODO: Need description here.'),
      //      '#description_display' => 'before',
      '#default_value' => $this->entity->get('paths') ?? $backstop_config->get('profile_parameters.paths'),
      '#attributes' => [
        //        'readonly' => $this->entity->get('use_globals') ?? TRUE,
        'class' => ['advanced-setting hidden-field'],
      ],
    ];

    $form['profile_settings']['report'] = [
      '#type' => 'textfield',
      //      '#title' => $this->t('Report'),
      //      '#description' => $this->t('TODO: Need description here.'),
      //      '#description_display' => 'before',
      '#default_value' => $this->entity->get('report') ?? $backstop_config->get('profile_parameters.report'),
      '#attributes' => [
        //        'readonly' => $this->entity->get('use_globals') ?? TRUE,
        'class' => ['advanced-setting'],
        'hidden' => TRUE,
      ],
    ];

    $form['profile_settings']['asyncCaptureLimit'] = [
      '#type' => 'number',
      '#title' => $this->t('asyncCaptureLimit'),
      '#description' => $this->t('TODO: Need description here.'),
      '#description_display' => 'before',
      '#default_value' => $this->entity->get('asyncCaptureLimit') ?? $backstop_config->get('profile_parameters.asyncCaptureLimit'),
      '#attributes' => [
        //        'readonly' => $this->entity->get('use_globals') ?? TRUE,
        'class' => ['advanced-setting'],
      ],
    ];

    $form['profile_settings']['asyncCompareLimit'] = [
      '#type' => 'number',
      '#title' => $this->t('asyncCompareLimit'),
      '#description' => $this->t('TODO: Need description here.'),
      '#description_display' => 'before',
      '#default_value' => $this->entity->get('asyncCompareLimit') ?? $backstop_config->get('profile_parameters.asyncCompareLimit'),
      '#attributes' => [
        //        'readonly' => $this->entity->get('use_globals') ?? TRUE,
        'class' => ['advanced-setting'],
      ],
    ];

    $form['debugging'] = [
      '#type' => 'details',
      '#title' => $this->t('Debugging'),
      '#open' => FALSE,
    ];

    $form['debugging']['debug'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('debug'),
      '#description' => $this->t('TODO: Need description here.'),
      '#description_display' => 'before',
      '#default_value' => $this->entity->get('debug') ?? $backstop_config->get('debug'),
      '#attributes' => [
        'readonly' => $this->entity->get('use_globals') ?? TRUE,
        'class' => ['advanced-setting'],
      ],
    ];

    $form['debugging']['debugWindow'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('debugWindow'),
      '#description' => $this->t('TODO: Need description here.'),
      '#description_display' => 'before',
      '#default_value' => $this->entity->get('debugWindow') ?? $backstop_config->get('debugWindow'),
      '#attributes' => [
        'readonly' => $this->entity->get('use_globals') ?? TRUE,
        'class' => ['advanced-setting'],
      ],
    ];

    $form['viewports'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Viewports'),
      '#description' => $this->t('Select the viewports to include in this profile'),
      '#description_display' => 'before',
      '#options' => $this->getConfig('backstop_viewport'),
      '#default_value' => $this->entity->get('viewports') ?? [],
      '#suffix' => $viewport_link->toString()->getGeneratedLink(),
    ];

    $form['scenarios'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Scenarios'),
      '#description' => $this->t('Select the scenarios to include in this profile.'),
      '#description_display' => 'before',
      '#options' => $this->getConfig('backstop_scenario'),
      '#default_value' => ($this->entity->get('scenarios')) ?? [],
      '#suffix' => $scenario_link->toString()->getGeneratedLink(),
    ];

    switch ($form_state->getValue('engine')) {
      case 'puppeteer':
        $form['profile_settings']['onBeforeScript']['#options'] = [
          '' => $this->t('Do not include'),
          'puppet/onBefore.js' => 'puppet/onBefore.js',
        ];
        $form['profile_settings']['onBeforeScript']['#default_value'] = '';
        break;

      case 'playwright':
        $form['profile_settings']['onBeforeScript']['#options'] = [
          '' => $this->t('Do not include'),
          'playwright/onBefore.js' => 'playwright/onBefore.js',
        ];
        $form['profile_settings']['onBeforeScript']['#default_value'] = '';
        break;
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $backstop_settings = $this->configFactory()->get('backstop_generator.settings');
    $profile_default_parameters = $backstop_settings->get('profile_parameters');
    $config = $this->configFactory->getEditable("backstop_generator.profile.{$this->entity->id()}");

    $result = parent::save($form, $form_state);

    // Reset config values to the global defaults if useProfileDefaults is true.
    if ($form_state->getValue('useProfileDefaults')) {
      foreach ($profile_default_parameters as $config_name => $config_value) {
        $config->set($config_name, $config_value);
        $this->entity->set($config_name, $config_value);
      }
      $config->set('engine', $backstop_settings->get('engine'));
      $config->save();
      $this->entity->set('engine', $backstop_settings->get('engine'));
      $this->entity->save();
    }

    $message_args = ['%label' => $this->entity->label()];
    $message = $result == SAVED_NEW
      ? $this->t('Created new backstop profile %label.', $message_args)
      : $this->t('Updated backstop profile %label.', $message_args);
    $this->messenger()->addStatus($message);
    $form_state->setRedirectUrl($this->entity->toUrl('collection'));
    $this->entity->generateBackstopFile();
    return $result;
  }

  /**
   * Returns a list of configuration entity names keyed by their ID.
   *
   * This method returns the list of configured Viewports/Scenarios.
   *
   * @param string $config_name
   *
   * @return array
   */
  protected function getConfig(string $config_name) {
    // Get the config entity manager.
    $entity_storage = \Drupal::service('entity_type.manager')
      ->getStorage($config_name);
    // Get the entity query object.
    $entity_query = $entity_storage->getQuery();
    // Get the viewport configs.
    $config_ids = $entity_query->execute();
    $configs = $entity_storage->loadMultiple($config_ids);

    // Create the array of configs.
    $config_list = [];
    foreach ($configs as $config) {
      if ($config_name == 'backstop_scenario') {
        $bundle = $config->get('bundle') ?? '';
        $config_list[$config->id()] = ucfirst($bundle) . ": {$config->label()}";
        continue;
      }
      $config_list[$config->id()] = $config->label();
    }
    asort($config_list);
    return $config_list;
  }

  /**
   * AJAX callback to modify the onBeforeScript options.
   *
   * @param $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *
   * @return \Drupal\Core\Ajax\AjaxResponse
   */
  public function modifyScriptOption($form, FormStateInterface $form_state) {
    $response = new AjaxResponse();

    // Update the profile_settings section in the form.
    $response->addCommand(new ReplaceCommand('#profile-settings', $form['profile_settings']));

    return $response;
  }

  /**
   * Returns the onBefore script options field.
   *
   * @param $value
   *   The name of the test engine (puppeteer || playwright).
   *
   * @return array
   *   The $form field array.
   */
  protected function onBeforeScriptOption ($value) {
    $field = [
      '#type' => 'radios',
      '#title' => $this->t('onBeforeScript'),
      '#description' => $this->t('Used to set up browser state e.g. cookies.'),
      '#description_display' => 'before',
//      '#default_value' => $this->entity->get('onBeforeScript') ?? $backstop_config->get('profile_parameters.onBeforeScript'),
      '#attributes' => [
        'class' => ['advanced-setting'],
      ],
    ];

    switch ($value) {
      case 'puppeteer':
        $field['#options'] = [
          '' => $this->t('Do not include'),
          'puppet/onBefore.js' => 'puppet/onBefore.js',
        ];
        break;

      case 'playwright':
        $field['#options'] = [
          '' => $this->t('Do not include'),
          'playwright/onBefore.js' => 'playwright/onBefore.js',
        ];
        break;
    }
    return $field;
  }

//  public function useProfileDefaults () {
//
//  }

}
