<?php

namespace Drupal\backstop_generator\Form;

use Drupal\backstop_generator\Entity\BackstopProfile;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\InvokeCommand;
use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\node\Entity\Node;

/**
 * Backstop Scenario form.
 *
 * @property \Drupal\backstop_generator\BackstopScenarioInterface $entity
 */
class BackstopScenarioForm extends EntityForm {

  /**
   * The field map used to modify values prior to saving.
   *
   * @var array
   */
  protected $field_map = [
    'label',
    'id',
    'url',
    'referenceUrl',
    'bundle',
    'delay',
    'useScenarioDefaults',
    'hideSelectors',
    'removeSelectors',
    'misMatchThreshold',
    'readyEvent',
    'readySelector',
    'readyTimeout',
    'onReadyScript',
    'onBeforeScript',
    'keyPressSelectors',
    'hoverSelector',
    'hoverSelectors',
    'clickSelector',
    'clickSelectors',
    'postInteractionWait',
    'cookiePath',
    'scrollToSelector',
    'selectors',
    'selectorExpansion',
    'expect',
    'goToParameters',
  ];

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);
    $server = $_SERVER;
    $site = "{$server['REQUEST_SCHEME']}://{$server['SERVER_NAME']}";
    $backstop_settings = $this->configFactory()->get('backstop_generator.settings');

    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#default_value' => $this->entity->get('label'),
      '#autocomplete_route_name' => 'backstop_generator.autocomplete',
      '#description' => t('select a label from the autocomplete.'),
      '#required' => TRUE,
      '#ajax' => [
        'callback' => '::populate',
        'event' => 'autocompleteclose',
      ],
      '#attributes' => [
        'disabled' => !$this->entity->isNew(),
      ],
    ];

    $form['id'] = [
      '#type' => 'textfield',
      '#default_value' => $this->entity->id(),
      '#attributes' => [
        'hidden' => 'hidden',
        'disabled' => !$this->entity->isNew(),
      ],
    ];

    $form['url'] = [
      '#type' => 'url',
      '#title' => $this->t('URL'),
      '#default_value' => $this->entity->get('url') ?? $site,
      '#description' => $this->t('The URL of the page you want to test in this scenario.'),
      '#description_display' => 'before',
      '#required' => TRUE,
      '#attributes' => [
        'placeholder' => 'http://dev-site.com/node/[nid]',
        'disabled' => !$this->entity->isNew(),
      ],
    ];

    $form['referenceUrl'] = [
      '#type' => 'textfield',
      '#title' => $this->t('referenceUrl'),
      '#default_value' => $this->entity->get('referenceUrl'),
      '#description' => $this->t('The domain you want to test against (source of truth) - no trailing slash.'),
      '#description_display' => 'before',
      '#attributes' => [
        'placeholder' => 'http://prod-site.com/node/[nid]',
      ],
    ];

    $form['bundle'] = [
      '#type' => 'textfield',
      '#default_value' => $this->entity->get('bundle'),
      '#attributes' => [
        'hidden' => 'hidden',
        'disabled' => !$this->entity->isNew(),
      ],
    ];

    $form['useScenarioDefaults'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Use scenarioDefaults'),
      '#default_value' => $this->entity->get('useScenarioDefaults') ?? FALSE,
      '#description' => $this->t('Unchecking this box will make the settings available.'),
    ];

    $form['scenario_settings'] = [
      '#type' => 'container',
      '#title' => $this->t('Scenario settings'),
      '#attributes' => ['id' => 'scenario-settings'],
      '#states' => [
        'visible' => [
          ':input[name="useScenarioDefaults"]' => ['checked' => FALSE],
        ],
      ],
    ];

    $form['scenario_settings']['basic'] = [
      '#type' => 'details',
      '#description' => $this->t('Modify these defaults to create a basic scenario for your profile.'),
      '#title' => $this->t('Basic Settings'),
      '#open' => TRUE,
    ];

    $form['scenario_settings']['advanced'] = [
      '#type' => 'details',
      '#description' => $this->t('Test more complex functionality in your profile.'),
      '#title' => $this->t('Advanced'),
      '#open' => FALSE,
      'dom_events' => [
        '#type' => 'details',
        '#title' => $this->t('DOM Events'),
        '#description' => $this->t('Modify the this scenarios based on DOM events.'),
        '#open' => FALSE,
      ],
      'user_behavior' => [
        '#type' => 'details',
        '#title' => $this->t('User Behaviors'),
        '#description' => $this->t('Test specific user behaviors.'),
        '#open' => FALSE,
      ],
    ];

    $form['scenario_settings']['basic']['delay'] = [
      '#type' => 'number',
      '#title' => $this->t('delay'),
      '#default_value' => $this->entity->get('delay') ?? $backstop_settings->get('scenario_defaults.delay'),
      '#description' => $this->t('Wait for x milliseconds.'),
      '#field_suffix' => 'ms',
      '#description_display' => 'before',
    ];

    $form['scenario_settings']['basic']['hideSelectors'] = [
      '#type' => 'textarea',
      '#title' => $this->t('hideSelectors'),
      '#default_value' => $this->entity->get('hideSelectors') ?? $backstop_settings->get('scenario_defaults.hideSelectors'),
      '#description' => $this->t('A comma-separated list of selectors you want to hide (visibility set to <em>hidden</em>).'),
      '#description_display' => 'before',
    ];

    $form['scenario_settings']['basic']['removeSelectors'] = [
      '#type' => 'textarea',
      '#title' => $this->t('removeSelectors'),
      '#default_value' => $this->entity->get('removeSelectors') ?? $backstop_settings->get('scenario_defaults.removeSelectors'),
      '#description' => $this->t('A comma-separated list of selectors you want to remove (display set to <em>none</em>).'),
      '#description_display' => 'before',
    ];

    $form['scenario_settings']['basic']['misMatchThreshold'] = [
      '#type' => 'number',
      '#title' => $this->t('misMatchThreshold'),
      '#min' => 0,
      '#max' => 100,
      '#step' => .01,
      '#default_value' => $this->entity->get('misMatchThreshold') ?? $backstop_settings->get('scenario_defaults.misMatchThreshold'),
      '#description' => $this->t('Percentage of different pixels allowed to pass test.'),
      '#description_display' => 'before',
      '#field_suffix' => '%',
    ];

    $form['scenario_settings']['basic']['requireSameDimensions'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('requireSameDimensions'),
      '#default_value' => $this->entity->get('requireSameDimensions') ?? $backstop_settings->get('scenario_defaults.requireSameDimensions'),
      '#description' => $this->t('Set to true, any change in selector size will trigger a test failure.'),
      '#return_value' => TRUE,
    ];

    $form['scenario_settings']['advanced']['cookiePath'] = [
      '#type' => 'textfield',
      '#title' => $this->t('cookiePath'),
      '#default_value' => $this->entity->get('cookiePath') ?? $backstop_settings->get('scenario_defaults.cookiePath'),
      '#description' => $this->t('Import cookies in JSON format to get around the <em>Accept Cookies</em> screen.'),
      '#description_display' => 'before',
    ];

    $form['scenario_settings']['advanced']['dom_events']['readyEvent'] = [
      '#type' => 'textfield',
      '#title' => $this->t('readyEvent'),
      '#default_value' => $this->entity->get('readyEvent') ?? $backstop_settings->get('scenario_defaults.readyEvent'),
      '#description' => $this->t('Wait until this string has been logged to the console.'),
      '#description_display' => 'before',
    ];

    $form['scenario_settings']['advanced']['dom_events']['readySelector'] = [
      '#type' => 'textfield',
      '#title' => $this->t('readySelector'),
      '#default_value' => $this->entity->get('readySelector') ?? $backstop_settings->get('scenario_defaults.readySelector'),
      '#description' => $this->t('Wait until this selector exists before continuing.'),
      '#description_display' => 'before',
    ];

    $form['scenario_settings']['advanced']['dom_events']['readyTimeout'] = [
      '#type' => 'number',
      '#title' => $this->t('readyTimeout'),
      '#default_value' => $this->entity->get('readyTimeout') ?? $backstop_settings->get('scenario_defaults.readyTimeout'),
      '#description' => $this->t('Timeout for readyEvent and readySelector.'),
      '#field_suffix' => 'ms',
      '#description_display' => 'before',
    ];

    $form['scenario_settings']['advanced']['dom_events']['onReadyScript'] = [
      '#type' => 'radios',
      '#title' => $this->t('onReadyScript'),
      '#default_value' => $this->entity->get('onReadyScript') ?? $backstop_settings->get('scenario_defaults.onReadyScript'),
      '#description' => $this->t('Script to modify UI state prior to screen shots e.g. hovers, clicks etc.'),
      '#description_display' => 'before',
      '#options' => [
        '' => $this->t('Do not include'),
        'onBefore.js' => $this->t('onBefore.js'),
      ],
    ];

    $form['scenario_settings']['advanced']['dom_events']['onBeforeScript'] = [
      '#type' => 'radios',
      '#title' => $this->t('onBeforeScript'),
      '#default_value' => $this->entity->get('onBeforeScript') ?? $backstop_settings->get('scenario_defaults.onBeforeScript'),
      '#description' => $this->t('Used to set up browser state e.g. cookies.'),
      '#description_display' => 'before',
      '#options' => [
        '' => $this->t('Do not include'),
        'onReady.js' => $this->t('onReady.js'),
      ],
    ];

    // todo: create a custom, multi-column field.
    $form['scenario_settings']['advanced']['user_behavior']['keyPressSelectors'] = [
      '#type' => 'textarea',
      '#title' => $this->t('keyPressSelectors'),
      '#default_value' => $this->entity->get('keyPressSelectors') ?? $backstop_settings->get('scenario_defaults.keyPressSelectors'),
      '#description' => $this->t('List of selectors to simulate multiple sequential keypress interactions.'),
      '#description_display' => 'before',
    ];

    $form['scenario_settings']['advanced']['user_behavior']['hoverSelector'] = [
      '#type' => 'textfield',
      '#title' => $this->t('hoverSelector'),
      '#default_value' => $this->entity->get('hoverSelector') ?? $backstop_settings->get('scenario_defaults.hoverSelector'),
      '#description' => $this->t('Move the pointer over the specified DOM element prior to screen shot.'),
      '#description_display' => 'before',
    ];

    $form['scenario_settings']['advanced']['user_behavior']['hoverSelectors'] = [
      '#type' => 'textarea',
      '#title' => $this->t('hoverSelectors'),
      '#default_value' => $this->entity->get('hoverSelectors') ?? $backstop_settings->get('scenario_defaults.hoverSelectors'),
      '#description' => $this->t('Selectors to simulate multiple sequential hover interactions.'),
      '#description_display' => 'before',
    ];

    $form['scenario_settings']['advanced']['user_behavior']['clickSelector'] = [
      '#type' => 'textfield',
      '#title' => $this->t('clickSelector'),
      '#default_value' => $this->entity->get('clickSelector') ?? $backstop_settings->get('scenario_defaults.clickSelector'),
      '#description' => $this->t('Click the specified DOM element prior to screen shot.'),
      '#description_display' => 'before',
    ];

    $form['scenario_settings']['advanced']['user_behavior']['clickSelectors'] = [
      '#type' => 'textarea',
      '#title' => $this->t('clickSelectors'),
      '#default_value' => $this->entity->get('clickSelectors') ?? $backstop_settings->get('scenario_defaults.clickSelectors'),
      '#description' => $this->t('Description of the backstop scenario.'),
      '#description_display' => 'before',
    ];

    $form['scenario_settings']['advanced']['user_behavior']['postInteractionWait'] = [
      '#type' => 'number',
      '#title' => $this->t('postInteractionWait'),
      '#default_value' => $this->entity->get('postInteractionWait') ?? $backstop_settings->get('scenario_defaults.postInteractionWait'),
      '#description' => $this->t('Wait for a selector after interacting with hoverSelector or clickSelector.'),
      '#description_display' => 'before',
    ];

    $form['scenario_settings']['advanced']['scrollToSelector'] = [
      '#type' => 'textfield',
      '#title' => $this->t('scrollToSelector'),
      '#default_value' => $this->entity->get('scrollToSelector') ?? $backstop_settings->get('scenario_defaults.scrollToSelector'),
      '#description' => $this->t('Scrolls the specified DOM element into view prior to screen shot.'),
      '#description_display' => 'before',
    ];

    $form['scenario_settings']['advanced']['selectors'] = [
      '#type' => 'textarea',
      '#title' => $this->t('selectors'),
      '#default_value' => $this->entity->get('selectors') ?? $backstop_settings->get('scenario_defaults.selectors'),
      '#description' => $this->t('List of selectors to capture.'),
      '#description_display' => 'before',
    ];

    $form['scenario_settings']['advanced']['selectorExpansion'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('selectorExpansion'),
      '#default_value' => $this->entity->get('selectorExpansion') ?? $backstop_settings->get('scenario_defaults.selectorExpansion'),
      '#description' => $this->t('Whether to take screenshots of designated selectors.'),
      '#return_value' => 1,
    ];

    $form['scenario_settings']['advanced']['expect'] = [
      '#type' => 'number',
      '#title' => $this->t('expect'),
      '#default_value' => $this->entity->get('expect') ?? $backstop_settings->get('scenario_defaults.expect'),
      '#description' => $this->t('The number of selector elements to test for.'),
      '#description_display' => 'before',
    ];

    // todo: custom field with multiple columns for goToParameter values.
    $form['scenario_settings']['advanced']['goToParameters'] = [
      '#type' => 'textarea',
      '#title' => $this->t('goToParameters'),
      '#default_value' => $this->entity->get('goToParameters') ?? $backstop_settings->get('scenario_defaults.goToParameters'),
      '#description' => $this->t('A list of settings passed to page.goto(url, parameters) function.'),
      '#description_display' => 'before',
    ];

//    switch ($backstop_settings->get('engine')) {
//      case 'puppeteer':
//        $form['scenario_settings']['advanced']['dom_events']['onBeforeScript']['#options'] = [
//          '' => $this->t('Do not include'),
//          'puppet/onBefore.js' => $this->t('puppet/onBefore.js'),
//        ];
//        $form['scenario_settings']['advanced']['dom_events']['onReadyScript']['#options'] = [
//          '' => $this->t('Do not include'),
//          'puppet/onReady.js' => $this->t('puppet/onReady.js'),
//        ];
//        break;
//
//      case 'playwright':
//        $form['scenario_settings']['advanced']['dom_events']['onBeforeScript']['#options'] = [
//          '' => $this->t('Do not include'),
//          'playwright/onBefore.js' => $this->t('playwright/onBefore.js'),
//        ];
//        $form['scenario_settings']['advanced']['dom_events']['onReadyScript']['#options'] = [
//          '' => $this->t('Do not include'),
//          'playwright/onReady.js' => $this->t('playwright/onReady.js'),
//        ];
//
//        break;
//    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state){

    // validate the value of the hidden 'bundle' and 'id' fields since they are populated when a label is selected
    $id = $form_state->getValue('id');
    $bundle = $form_state->getValue('bundle');

    if($id === '' || $bundle === ''){
      $form_state->setErrorByName('label', $this->t('Please add a valid label from the autocomplete.'));
    }

  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $backstop_settings = $this->configFactory()->get('backstop_generator.settings');
    $scenario_defaults = $backstop_settings->get('scenario_defaults');
    $config = $this->configFactory->getEditable("backstop_generator.scenario.{$this->entity->id()}");

    $result = parent::save($form, $form_state);

    // Remove this scenario's settings if using scenarioDefaults.
    if ($form_state->getValue('useScenarioDefaults')) {
      foreach ($scenario_defaults as $config_name => $config_value) {
        $config->set($config_name, NULL);
      }
      $config->save();
    }

    $message_args = ['%label' => $this->entity->label()];
    $message = $result == SAVED_NEW
      ? $this->t('Created new backstop scenario %label.', $message_args)
      : $this->t('Updated backstop scenario %label.', $message_args);
    $this->messenger()->addStatus($message);
    $form_state->setRedirectUrl($this->entity->toUrl('collection'));

    $updated_profiles = $this->updateProfiles();
    $update_message = count($updated_profiles) > 0 ?
      $this->t('Updated %label backstop.json profile file.', ['%label' => implode(', ', $updated_profiles)]) :
      $this->t('No profiles needed to be updated.');
    $this->messenger()->addMessage($update_message);

    return $result;
  }

  /**
   * AJAX callback: populates hidden/readonly fields based on label field input.
   *
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *
   * @return \Drupal\Core\Ajax\AjaxResponse
   */
  public function populate(array $form, FormStateInterface $form_state) {
    $response = new AjaxResponse();

    $label = $form_state->getValue('label');
    // Isolate the nid from the label field.
    preg_match('/([\w\s]+) \((\d+)\)$/', $label, $nid);
    $node = Node::load($nid[2]);

    if (!empty($label) && isset($nid[1])) {
      // Set the value of the bundle field.
      $response->addCommand(new InvokeCommand('#edit-bundle', 'val', [$node->bundle()]));
      // Set the value of url using the nid value.
      if (!preg_match('/node\/\d+$/', $this->entity->get('url'))) {
        $response->addCommand(new InvokeCommand('#edit-url', 'val', ["{$this->entity->get('url')}/node/$nid[2]"]));
      }
      else {
        preg_match('/([\:\w\.\/-]+)\/\d+$/', $this->entity->get('url'), $url);
        $response->addCommand(new InvokeCommand('#edit-url', 'val', ["$url[1]/$nid[2]"]));
      }
      // Set the value of id to the path.
      $response->addCommand(new InvokeCommand('#edit-id', 'val', ["node-$nid[2]"]));
    }
    return $response;
  }

  /**
   * Updates test Profiles used by this Scenario.
   *
   * @return array
   *   An indexed array of updated profiles.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  private function updateProfiles() {
    // Get the profile config ids.
    $profile_ids = \Drupal::entityTypeManager()
      ->getStorage('backstop_profile')
      ->getQuery()
      ->execute();
    $updated_profiles = [];

    foreach ($profile_ids as $id) {
      // Get the profile config.
      $profile_config = \Drupal::configFactory()->getEditable("backstop_generator.profile.$id");
      if (in_array($this->entity->id(), $profile_config->get('scenarios'), TRUE)) {
        // Update the backstop.json file.
        $profile = BackstopProfile::load($id);
        $profile->generateBackstopFile($id);
        $updated_profiles[] = $profile->label();
      }
    }
    return $updated_profiles;
  }

}
