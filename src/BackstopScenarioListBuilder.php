<?php

namespace Drupal\backstop_generator;

use Drupal\Core\Config\Entity\ConfigEntityListBuilder;
use Drupal\Core\Entity\EntityInterface;
use Drupal\backstop_generator\Form\BackstopScenarioFilterForm;

/**
 * Provides a listing of backstop scenarios.
 */
class BackstopScenarioListBuilder extends ConfigEntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['label'] = $this->t('Label');
    $header['id'] = $this->t('ID');
    $header['bundle'] = $this->t('Bundle');

    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /** @var \Drupal\backstop_generator\BackstopScenarioInterface $entity */
    $row['label'] = $entity->label();
    $row['id'] = $entity->id();
    $row['bundle'] = $entity->get('bundle');

    return  $row + parent::buildRow($entity);
  }

  /**
   * {@inheritdoc}
   */
  public function render() {
    // Initialize an empty render array.
    $build = [];

    // Add the filter form directly to the render array.
    $build['filter_form'] = \Drupal::formBuilder()->getForm(BackstopScenarioFilterForm::class);;

    // Add the parent render array, which includes the entity listing.
    $build += parent::render();

    return $build;
  }

  /**
   * {@inheritdoc}
   */
  protected function getEntityIds() {
    // Get all entity IDs.
    $entity_ids = \Drupal::entityTypeManager()
      ->getStorage($this->entityTypeId)
      ->getQuery()
      ->execute();

    // Check if a label filter is applied.
    $label_filter = \Drupal::request()->query->get('label_filter');
    if (!empty($label_filter)) {
      // Load all entities and manually filter them based on the label.
      $entities = \Drupal::entityTypeManager()
        ->getStorage($this->entityTypeId)
        ->loadMultiple($entity_ids);

      // Filter entities manually.
      $filtered_entity_ids = [];
      foreach ($entities as $entity) {
        if (stripos($entity->label(), $label_filter) !== FALSE) {
          $filtered_entity_ids[] = $entity->id();
        }
      }
      return $filtered_entity_ids;
    }
    return $entity_ids;
  }
}
