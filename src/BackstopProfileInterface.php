<?php

namespace Drupal\backstop_generator;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface defining a backstop profile entity type.
 */
interface BackstopProfileInterface extends ConfigEntityInterface {

  /**
   * Generates a backstop file from configuration settings.
   *
   * @return mixed
   */
  public function generateBackstopFile();
}
