<?php

namespace Drupal\backstop_generator;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface defining a backstop scenario entity type.
 */
interface BackstopScenarioInterface extends ConfigEntityInterface {

}
