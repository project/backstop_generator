<?php

namespace Drupal\backstop_generator\Services;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Menu\MenuLinkTreeInterface;
use Drupal\Core\Menu\MenuTreeParameters;
use Drupal\node\Entity\Node;

/**
 * Service description.
 */
class MenuNodeData {

  /**
   *
   * The list of menu paths.
   *
   * @var array
   */
  protected $menu_paths = [];

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The menu link tree service.
   *
   * @var \Drupal\Core\Menu\MenuLinkTreeInterface
   */
  protected $menuLinkTree;

  /**
   * Constructs a MenuNodeData object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Menu\MenuLinkTreeInterface $menu_link_tree
   *   The menu link tree service.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, MenuLinkTreeInterface $menu_link_tree) {
    $this->entityTypeManager = $entity_type_manager;
    $this->menuLinkTree = $menu_link_tree;
  }

  /**
   * Returns the paths for all the links in a given menu up to the given depth.
   *
   * @param string $menu_id
   * @param int $level
   *
   * @return array
   */
  public function getMenuLinkPaths($menu_id, $level = 1) {
    // Ensure the level is at least 1.
    $level = max(1, (int)$level);

    // Load the menu tree service.
    $menu_tree = \Drupal::menuTree();

    // Create an instance of MenuTreeParameters.
    $parameters = new MenuTreeParameters();
    $parameters->minDepth = 1;
    $parameters->maxDepth = $level;

    // Load the menu tree based on the parameters.
    $tree = $menu_tree->load($menu_id, $parameters);

    // Transform the tree into a renderable array.
    $manipulators = [
      ['callable' => 'menu.default_tree_manipulators:checkAccess'],
      ['callable' => 'menu.default_tree_manipulators:generateIndexAndSort'],
    ];
    $tree = $menu_tree->transform($tree, $manipulators);

    // Traverse the tree and collect menu paths.
    foreach ($tree as $element) {
      $this->menu_paths = array_merge($this->menu_paths, $this->collectMenuPathsFromTree($element));
    }

    return $this->menu_paths;
  }

  /**
   * Helper function to recursively collect paths from a menu tree element.
   *
   * @param \Drupal\Core\Menu\MenuLinkTreeElement $element
   *   The menu tree element to process.
   *
   * @return array
   *   An array of paths collected from the menu tree element.
   */
  protected function collectMenuPathsFromTree(\Drupal\Core\Menu\MenuLinkTreeElement $element) {
    $menu_paths = [];

    // Get the alias/path for the current menu item.
    if ($element->link->getUrlObject() instanceof \Drupal\Core\Url) {
      $route_parameter_object = $element->link->getUrlObject();

      // Ignore external, empty and disabled menu links; inform the user why.
      if (!$element->link->isEnabled()) {
        $link_title = $element->link->getTitle();
        $message = t("MENU LINK IGNORED: The %title menu link was ignored because it is disabled.", ['%title' => $link_title]);
        \Drupal::messenger()->addStatus($message);
      }
      // Handle paths being redirected.
      elseif (!$route_parameter_object->isRouted() && !$route_parameter_object->isExternal()) {
        $menu_paths[] = [
          'node' => 'redirect_url',
          'title' => $element->link->getTitle(),
          'path' => $element->link->getUrlObject()->toString(),
          'bundle' => 'redirect_url',
        ];
      }
      elseif (
        $route_parameter_object->isExternal() ||
        $route_parameter_object->getRouteName() == '<nolink>'
      ) {
        $link_title = $element->link->getTitle();
        $status = $route_parameter_object->isExternal() ?
          'links to an external website.' :
          'is routed to <nolink>.';
        $message = t("MENU LINK IGNORED: The %title menu link was ignored because it %status.", ['%title' => $link_title, '%status' => $status]);
        \Drupal::messenger()->addStatus($message);
        return [];
      }
      else {
        // Retrieve the bundle type.
        $route_params = $route_parameter_object->getRouteParameters();
        if (isset($route_params['node'])) {
          $node_id = (int) $route_params['node'];
          $node = Node::load($node_id);
          $bundle = $node->bundle();
        }

        // Create the data array.
        $alias = $element->link->getUrlObject()->toString();
        $title = $element->link->getTitle();
        $menu_paths[] = [
          'nid' => $node_id ?? NULL,
          'title' => $title,
          'path' => $alias,
          'bundle' => $bundle ?? NULL,
        ];
      }
    }

    // Recursively collect paths from child elements.
    if (!empty($element->subtree)) {
      foreach ($element->subtree as $child) {
        $menu_paths = array_merge($menu_paths, $this->collectMenuPathsFromTree($child));
      }
    }

    return $menu_paths;
  }

}
