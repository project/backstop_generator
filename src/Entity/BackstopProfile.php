<?php

namespace Drupal\backstop_generator\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;
use Drupal\backstop_generator\BackstopProfileInterface;
use Drupal\Core\Entity\Annotation\ConfigEntityType;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeManager;
use Drupal\Core\File\FileSystemInterface;

/**
 * Defines the backstop profile entity type.
 *
 * @ConfigEntityType(
 *   id = "backstop_profile",
 *   label = @Translation("Backstop Profile"),
 *   label_collection = @Translation("Backstop Profiles"),
 *   label_singular = @Translation("backstop profile"),
 *   label_plural = @Translation("backstop profiles"),
 *   label_count = @PluralTranslation(
 *     singular = "@count backstop profile",
 *     plural = "@count backstop profiles",
 *   ),
 *   handlers = {
 *     "list_builder" = "Drupal\backstop_generator\BackstopProfileListBuilder",
 *     "form" = {
 *       "add" = "Drupal\backstop_generator\Form\BackstopProfileForm",
 *       "edit" = "Drupal\backstop_generator\Form\BackstopProfileForm",
 *       "delete" = "Drupal\backstop_generator\Form\BackstopProfileDeleteForm",
 *       "express" = "Drupal\backstop_generator_express\Form\BackstopProfileExpressForm"
 *     }
 *   },
 *   config_prefix = "profile",
 *   admin_permission = "administer backstop_generator",
 *   links = {
 *     "collection" = "/admin/config/development/backstop-generator/profiles",
 *     "add-form" = "/admin/config/development/backstop-generator/profile/add",
 *     "edit-form" = "/admin/config/development/backstop-generator/profile/{backstop_profile}",
 *     "delete-form" = "/admin/config/development/backstop-generator/profile/{backstop_profile}/delete",
 *     "express-form" = "/admin/config/development/backstop-generator/profile/express"
 *   },
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "description",
 *     "useProfileDefaults",
 *     "viewports",
 *     "onBeforeScript",
 *     "scenarios",
 *     "paths",
 *     "report",
 *     "engine",
 *     "engineOptions",
 *     "asyncCaptureLimit",
 *     "asyncCompareLimit",
 *     "debug",
 *     "debugWindow"
 *   }
 * )
 */
class BackstopProfile extends ConfigEntityBase implements BackstopProfileInterface {

  /**
   * The backstop profile ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The backstop profile label.
   *
   * @var string
   */
  protected $label;

  /**
   * The backstop profile status.
   *
   * @var bool
   */
  protected $status;

  /**
   * The backstop_profile description.
   *
   * @var string
   */
  protected $description;

  /**
   * Whether to use backstop default settings.
   *
   * @var bool
   */
  protected $useProfileDefaults = TRUE;

  /**
   * List of viewports included in this profile.
   *
   * @var array
   */
  protected $viewports;

  /**
   *
   *
   * @var string
   */
  protected $onBeforeScript;

  /**
   * The list of scenarios (pages) in this profile.
   *
   * @var array
   */
  protected $scenarios;

  /**
   *
   *
   * @var string
   */
  protected $paths;

  /**
   *
   *
   * @var string
   */
  protected $profile;

  /**
   *
   *
   * @var string
   */
  protected $engine;

  /**
   *
   *
   * @var string
   */
  protected $engineOptions;

  /**
   *
   *
   * @var int
   */
  protected $asyncCaptureLimit;

  /**
   *
   *
   * @var int
   */
  protected $asyncCompareLimit;

  /**
   *
   *
   * @var bool
   */
  protected $debug;

  /**
   *
   *
   * @var bool
   */
  protected $debugWindow;

  /**
   * @inheritdoc
   */
  public function generateBackstopFile($id = NULL) {
    $backstop = new \stdClass();
    $viewport_entities = $this->getConfigEntities('backstop_viewport');
    $scenario_entities = $this->getConfigEntities('backstop_scenario');
    $backstop_config = \Drupal::config('backstop_generator.settings');
    $profile_config = $id ? \Drupal::config("backstop_profile.$id") : NULL;

    $backstop->id = $this->id();

    // Create the viewports array.
    $viewports = [];
    foreach ($this->viewports as $vkey => $vid) {
      if ($vid === 0) {
        continue;
      }
      $viewport_entity = $viewport_entities->load($vid);
      $viewport = new \stdClass();
      $viewport->label = $vid;
      $viewport->width = $viewport_entity->get('width');
      $viewport->height = $viewport_entity->get('height');
      $viewports[] = $viewport;
    }
    $backstop->viewports = $viewports;

    // For the profile.
    $backstop->scenarioDefaults = $backstop_config->get('scenario_defaults');

    // Scenario config keys that require special handling.
    $ignore = ['uuid','langcode','status','dependencies','page','id','locked','description','bundle','useScenarioDefaults'];
    $array_vals = ['hideSelectors', 'removeSelectors','keyPressSelectors','hoverSelectors','clickSelectors','selectors','viewports','gotoParameters'];

    // Remove any empty or null values from the profile.
    foreach ($backstop->scenarioDefaults as $k => $v) {
      if ($v === '' || $v === NULL) {
        unset($backstop->scenarioDefaults[$k]);
      } else {
        if (in_array($k, $array_vals)) {
          $backstop->scenarioDefaults[$k] = $this->prepareArray($backstop->scenarioDefaults[$k]);
        }
      }
    }

    // Create the scenarios array.
    $scenarios = [];
    // Raw values for processing.
    $scenario_default_raw_values = $backstop_config->get('scenario_defaults');

    foreach ($this->scenarios as $key => $id) {
      if ($id === 0) {
        continue;
      }
      $scenario_config = $scenario_entities->load($id)?->toArray();
      $scenario = new \stdClass();

      if ($scenario_config) { // && !$scenario_config['useScenarioDefaults']
        foreach ($scenario_config as $config_key => $value) {

          // Ignore empty values, or values equal to the one used in the scenario default.
          if (
            $value === null ||
            $value === '' ||
            in_array($config_key, $ignore) ||
            (isset($backstop->scenarioDefaults[$config_key]) && $scenario_default_raw_values[$config_key] == $value)
          ) {
            continue;
          }

          // Add any values that are not used in the scenario default values.
          if (
            !$scenario_config['useScenarioDefaults'] &&
            (!isset($backstop->scenarioDefaults[$config_key]) || (isset($backstop->scenarioDefaults[$config_key]) && $scenario_default_raw_values[$config_key] != $value))
          )
          {
//            // Set the onBeforeScript and onReadyScript values.
//            if ($config_key == 'onBeforeScript' || $config_key == 'onReadyScript') {
//              if ($backstop->scenarioDefaults[$config_key]) {
//                $script = $config_key == 'onBeforeScript' ? 'puppet/onBefore.js' : 'puppet/onReady.js';
//                $scenario->$config_key = $script;
//                continue;
//              }
//              else {
//                unset($scenario->$config_key);
//              }
//            }
            if (in_array($config_key, $array_vals)) {
              $scenario->$config_key = $this->prepareArray($value);
            } else {
              // Include the engine for onReady and onBefore script paths.
              if ($config_key == 'onReadyScript' || $config_key == 'onBeforeScript') {
                $engine = $backstop->engine == 'puppeteer' ? 'puppet' : $backstop->engine;
                $scenario->$config_key = "$engine/$value";
              }
              else {
                $scenario->$config_key = $value;
              }

            }
          }

          // Add the basic scenario values.
          else {
//            if ($backstop->scenarioDefaults[$config_key] != $value) {
//              if (in_array($config_key, $array_vals) && isset($backstop->scenarioDefaults[$config_key])) {
////                $value_array = array_map('trim', explode(',', $backstop->scenarioDefaults[$config_key]));
//                $scenario->$config_key = $this->prepareArray($backstop->scenarioDefaults[$config_key]);
//              } else {
                $scenario->$config_key = $value;
//              }
//            }
          }

        }
      }
      $scenarios[] = $scenario;
    }
    $backstop->scenarios = $scenarios;
    // Process the scenario default values to get arrays into proper format.
//    foreach ($backstop->scenarioDefaults as $scenario_key => $scenario_value) {
//      if (in_array($scenario_key, $array_vals)) {
//        $value_array = array_map('trim', explode(',', $backstop->scenarioDefaults[$scenario_key]));
//        $backstop->scenarioDefaults[$scenario_key] = $value_array;
//      }
//    }

    // Create the paths object.
    $paths_value = $this->paths ?? $backstop_config->get('profile_parameters.paths');
    $paths_array = explode(PHP_EOL, $paths_value);
    $paths = new \stdClass();

    foreach ($paths_array as $path) {
      if (!empty($path)) {
        preg_match('/(\w+)\|([\w\/]+)/', $path, $path_parts);
        $paths->{$path_parts[1]} = $path_parts[2];
      }
    }
    $backstop->paths = $paths;

    $report_value = $this->report ?? $backstop_config->get('profile_parameters.report');
    $backstop->report = explode(',', $report_value);

    $backstop->engine = $this->engine ?? $backstop_config->get('profile_parameters.engine');
    $engineOptions_value = $this->engineOptions ?? $backstop_config->get('profile_parameters.engineOptions');
    $engineOptions = new \stdClass();
    $engineOptions->args = explode(',', $engineOptions_value);
    $backstop->engineOptions = $engineOptions;

    $backstop->asyncCaptureLimit = $this->asyncCaptureLimit ?? $backstop_config->get('profile_parameters.asyncCaptureLimit');
    $backstop->asyncCompareLimit = $this->asyncCompareLimit ?? $backstop_config->get('profile_parameters.asyncCompareLimit');
    $debug_value = $this->debug ?? $backstop_config->get('profile_parameters.debug');
    $backstop->debug = $debug_value == 1 ? true : false;
    $debugWindow_value = $this->debugWindow ?? $backstop_config->get('profile_parameters.debugWindow');
    $backstop->debugWindow = $debugWindow_value == 1 ? true : false;

    // Create and save the backstop.json file.
    // Create the profile directory.
    /** @var \Drupal\Core\File\FileSystemInterface $file_system */
    $file_system = \Drupal::service('file_system');
    $project_root = dirname(DRUPAL_ROOT);
    $filename = $this->id == 'default' ? 'backstop.json' : "$this->id.json";
//    $profile_directory = $this->id == 'default' ?
//      "$project_root/{$backstop_config->get('backstop_directory')}" :
//      "$project_root/{$backstop_config->get('backstop_directory')}/$this->id";
    $profile_directory = "$project_root/{$backstop_config->get('backstop_directory')}";
    $file_system->prepareDirectory($profile_directory, FileSystemInterface::CREATE_DIRECTORY | FileSystemInterface::MODIFY_PERMISSIONS);

    // Create, write and save the backstop file.
    $backstop_file = fopen("$profile_directory/$filename", "w");
    fwrite($backstop_file, json_encode($backstop, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE | JSON_NUMERIC_CHECK));
    fclose($backstop_file);

  }

  /**
   * Return the storage interface for the specified config entity.
   *
   * @param string $config_name
   *
   * @return \Drupal\Core\Entity\EntityStorageInterface
   */
  private function getConfigEntities(string $config_name): EntityStorageInterface {
    // Get the config entity manager.
    return \Drupal::service('entity_type.manager')
      ->getStorage($config_name);
  }

  private function prepareArray(string $value) {
    return array_map('trim', explode(',', $value));
  }

}
