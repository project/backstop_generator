<?php

namespace Drupal\backstop_generator;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface defining a backstop viewport entity type.
 */
interface BackstopViewportInterface extends ConfigEntityInterface {

}
