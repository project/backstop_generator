(function ($, Drupal, once) {
  Drupal.behaviors.customPathHandler = {
    attach: function (context, settings) {
      // Use the once utility from the once library to ensure the event is attached only once.
      once('custom-path-handler', '#add-path-to-list', context).forEach(function (element) {
        $(element).on('click', function (event) {
          // Prevent the default form submission action.
          event.preventDefault();

          var nodeTitleInput = $('#node-title');

          // Extract the node title and ID from the string, assuming the format "Title (ID)"
          var match = nodeTitleInput.val().match(/^(.*)\s\((\d+)\)$/);
          if (match) {
            var nodeTitle = match[1]; // The title part
            var nodeId = match[2]; // The ID part

            // Format the output as "Title:node/ID"
            var formattedPath = nodeTitle + ' | node/' + nodeId;

            // Get current textarea value and append the new path
            var currentPaths = $('#path-list-wrapper textarea').val();
            var updatedPaths = currentPaths ? currentPaths + "\n" + formattedPath : formattedPath;

            // Update the textarea
            $('#path-list-wrapper textarea').val(updatedPaths);

            // Select the content of the nodeTitleInput field so the user can start typing again
            nodeTitleInput.val('');
            nodeTitleInput.focus().select();
          } else {
            console.log('No valid node ID found in the input');
          }
        });
      });
    }
  };
})(jQuery, Drupal, once);
