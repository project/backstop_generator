(function ($, Drupal, once) {
  Drupal.behaviors.ajaxUpdateStates = {
    attach: function (context, settings) {
      // Ensure #states re-applies after AJAX loads.
      once('ajax-update-states', '#scenario-fields-wrapper', context).forEach(function (element) {
        // Re-trigger the #states logic to ensure it runs after the values are updated.
        Drupal.states.triggerDependent($(element));
      });
    }
  };
})(jQuery, Drupal, once);
