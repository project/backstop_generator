# Backstop Generator Module
Documentation for how to set up and use Backstop Generator.
## Installing BackstopJS
A brief statement about BackstopJS as it relates to this module and a link to the BackstopJS install page.
## Settings
Info on each of the settings.
## Viewports
Info about Viewports.
## Scenarios
Information about the various settings for Scenarios.
## Profiles
Information about the settings for Profiles.
## How to run Backstop tests
Tutorial for Backstop newbies.
